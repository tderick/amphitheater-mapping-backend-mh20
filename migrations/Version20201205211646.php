<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201205211646 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE mh_campus (id INT AUTO_INCREMENT NOT NULL, institution_id INT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_B17D99BF10405986 (institution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mh_classrooms (id INT AUTO_INCREMENT NOT NULL, campus_id INT NOT NULL, name VARCHAR(255) NOT NULL, capacity INT DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_F5C0FA02AF5D55E1 (campus_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mh_departements (id INT AUTO_INCREMENT NOT NULL, faculty_id INT NOT NULL, name VARCHAR(255) NOT NULL, headman VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_4C3C088E680CAB68 (faculty_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mh_faculties (id INT AUTO_INCREMENT NOT NULL, institution_id INT NOT NULL, name VARCHAR(255) NOT NULL, dean_name VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_93B6717C10405986 (institution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mh_images (id INT AUTO_INCREMENT NOT NULL, institution_id INT NOT NULL, campus_id INT NOT NULL, classroom_id INT NOT NULL, faculty_id INT NOT NULL, name VARCHAR(255) NOT NULL, src VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_CC6B4FC410405986 (institution_id), INDEX IDX_CC6B4FC4AF5D55E1 (campus_id), INDEX IDX_CC6B4FC46278D5A8 (classroom_id), INDEX IDX_CC6B4FC4680CAB68 (faculty_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mh_institutions (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, localisation VARCHAR(255) NOT NULL, pobox VARCHAR(255) NOT NULL, vice_chancellor VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mh_levels (id INT AUTO_INCREMENT NOT NULL, speciality_id INT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_B35E95B73B5A08D7 (speciality_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mh_periods (id INT AUTO_INCREMENT NOT NULL, classroom_id INT DEFAULT NULL, level_id INT DEFAULT NULL, begin_hour VARCHAR(50) NOT NULL, end_hour VARCHAR(50) NOT NULL, course VARCHAR(255) NOT NULL, state TINYINT(1) NOT NULL, official TINYINT(1) NOT NULL, day VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_565562BC6278D5A8 (classroom_id), INDEX IDX_565562BC5FB14BA7 (level_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mh_specialities (id INT AUTO_INCREMENT NOT NULL, department_id INT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_7CE76A2DAE80F5DF (department_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `mh_users` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, UNIQUE INDEX UNIQ_B9EC8057E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mh_campus ADD CONSTRAINT FK_B17D99BF10405986 FOREIGN KEY (institution_id) REFERENCES mh_institutions (id)');
        $this->addSql('ALTER TABLE mh_classrooms ADD CONSTRAINT FK_F5C0FA02AF5D55E1 FOREIGN KEY (campus_id) REFERENCES mh_campus (id)');
        $this->addSql('ALTER TABLE mh_departements ADD CONSTRAINT FK_4C3C088E680CAB68 FOREIGN KEY (faculty_id) REFERENCES mh_faculties (id)');
        $this->addSql('ALTER TABLE mh_faculties ADD CONSTRAINT FK_93B6717C10405986 FOREIGN KEY (institution_id) REFERENCES mh_institutions (id)');
        $this->addSql('ALTER TABLE mh_images ADD CONSTRAINT FK_CC6B4FC410405986 FOREIGN KEY (institution_id) REFERENCES mh_institutions (id)');
        $this->addSql('ALTER TABLE mh_images ADD CONSTRAINT FK_CC6B4FC4AF5D55E1 FOREIGN KEY (campus_id) REFERENCES mh_campus (id)');
        $this->addSql('ALTER TABLE mh_images ADD CONSTRAINT FK_CC6B4FC46278D5A8 FOREIGN KEY (classroom_id) REFERENCES mh_classrooms (id)');
        $this->addSql('ALTER TABLE mh_images ADD CONSTRAINT FK_CC6B4FC4680CAB68 FOREIGN KEY (faculty_id) REFERENCES mh_faculties (id)');
        $this->addSql('ALTER TABLE mh_levels ADD CONSTRAINT FK_B35E95B73B5A08D7 FOREIGN KEY (speciality_id) REFERENCES mh_specialities (id)');
        $this->addSql('ALTER TABLE mh_periods ADD CONSTRAINT FK_565562BC6278D5A8 FOREIGN KEY (classroom_id) REFERENCES mh_classrooms (id)');
        $this->addSql('ALTER TABLE mh_periods ADD CONSTRAINT FK_565562BC5FB14BA7 FOREIGN KEY (level_id) REFERENCES mh_levels (id)');
        $this->addSql('ALTER TABLE mh_specialities ADD CONSTRAINT FK_7CE76A2DAE80F5DF FOREIGN KEY (department_id) REFERENCES mh_departements (id)');
        $this->addSql('ALTER TABLE institution_user ADD CONSTRAINT FK_9B90060210405986 FOREIGN KEY (institution_id) REFERENCES mh_institutions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE institution_user ADD CONSTRAINT FK_9B900602A76ED395 FOREIGN KEY (user_id) REFERENCES `mh_users` (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mh_classrooms DROP FOREIGN KEY FK_F5C0FA02AF5D55E1');
        $this->addSql('ALTER TABLE mh_images DROP FOREIGN KEY FK_CC6B4FC4AF5D55E1');
        $this->addSql('ALTER TABLE mh_images DROP FOREIGN KEY FK_CC6B4FC46278D5A8');
        $this->addSql('ALTER TABLE mh_periods DROP FOREIGN KEY FK_565562BC6278D5A8');
        $this->addSql('ALTER TABLE mh_specialities DROP FOREIGN KEY FK_7CE76A2DAE80F5DF');
        $this->addSql('ALTER TABLE mh_departements DROP FOREIGN KEY FK_4C3C088E680CAB68');
        $this->addSql('ALTER TABLE mh_images DROP FOREIGN KEY FK_CC6B4FC4680CAB68');
        $this->addSql('ALTER TABLE mh_campus DROP FOREIGN KEY FK_B17D99BF10405986');
        $this->addSql('ALTER TABLE mh_faculties DROP FOREIGN KEY FK_93B6717C10405986');
        $this->addSql('ALTER TABLE mh_images DROP FOREIGN KEY FK_CC6B4FC410405986');
        $this->addSql('ALTER TABLE institution_user DROP FOREIGN KEY FK_9B90060210405986');
        $this->addSql('ALTER TABLE mh_periods DROP FOREIGN KEY FK_565562BC5FB14BA7');
        $this->addSql('ALTER TABLE mh_levels DROP FOREIGN KEY FK_B35E95B73B5A08D7');
        $this->addSql('ALTER TABLE institution_user DROP FOREIGN KEY FK_9B900602A76ED395');
        $this->addSql('DROP TABLE mh_campus');
        $this->addSql('DROP TABLE mh_classrooms');
        $this->addSql('DROP TABLE mh_departements');
        $this->addSql('DROP TABLE mh_faculties');
        $this->addSql('DROP TABLE mh_images');
        $this->addSql('DROP TABLE mh_institutions');
        $this->addSql('DROP TABLE mh_levels');
        $this->addSql('DROP TABLE mh_periods');
        $this->addSql('DROP TABLE mh_specialities');
        $this->addSql('DROP TABLE `mh_users`');
    }
}
