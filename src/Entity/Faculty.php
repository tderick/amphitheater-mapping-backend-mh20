<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\FacultyRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=FacultyRepository::class)
 * @ORM\Table(name="mh_faculties")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"faculty_read"}},
 *      denormalizationContext={"groups"={"faculty_write"}},
 *      
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 */
class Faculty
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"institution_read","faculty_read","department_read","faculty_write","department_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"institution_read","faculty_read","department_read","faculty_write"})
     * 
     * @Assert\NotBlank(message="The name of faculty cannot be blank")
     * @Assert\NotNull(message="The name of faculty is mandatory")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"institution_read","faculty_read","department_read","faculty_write"})
     * 
     * @Assert\NotBlank(message="The name of Dean cannot be blank")
     * @Assert\NotNull(message="The name of Dean is mandatory")
     */
    private $deanName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"institution_read","faculty_read","department_read","faculty_write"})
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"institution_read","faculty_read","department_read","faculty_write"})
     */
    private $logo;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="faculty", orphanRemoval=true)
     * @Groups({"faculty_read","faculty_write"})
     */
    private $images;

    /**
     * @ORM\ManyToOne(targetEntity=Institution::class, inversedBy="faculties")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"faculty_read","faculty_write"})
     * 
     * @Assert\NotNull(message="You must provide the institution of the faculty")
     */
    private $institution;

    /**
     * @ORM\OneToMany(targetEntity=Department::class, mappedBy="faculty", orphanRemoval=true)
     * @Groups({"faculty_read"})
     */
    private $departments;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->departments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDeanName(): ?string
    {
        return $this->deanName;
    }

    public function setDeanName(string $deanName): self
    {
        $this->deanName = $deanName;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setFaculty($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getFaculty() === $this) {
                $image->setFaculty(null);
            }
        }

        return $this;
    }

    public function getInstitution(): ?Institution
    {
        return $this->institution;
    }

    public function setInstitution(?Institution $institution): self
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * @return Collection|Department[]
     */
    public function getDepartments(): Collection
    {
        return $this->departments;
    }

    public function addDepartment(Department $department): self
    {
        if (!$this->departments->contains($department)) {
            $this->departments[] = $department;
            $department->setFaculty($this);
        }

        return $this;
    }

    public function removeDepartment(Department $department): self
    {
        if ($this->departments->removeElement($department)) {
            // set the owning side to null (unless already changed)
            if ($department->getFaculty() === $this) {
                $department->setFaculty(null);
            }
        }

        return $this;
    }
}
