<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\PeriodRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=PeriodRepository::class)
 * @ORM\Table(name="mh_periods")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"period_read"}},
 *      denormalizationContext={"groups"={"period_write"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * 
 */
class Period
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"classroom_read","level_read","period_read","period_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"classroom_read","level_read","period_read","period_write"})
     * 
     * @Assert\NotBlank(message="The begin hour cannot be blank")
     * @Assert\NotNull(message="The begin hour is mandatory")
     */
    private $beginHour;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"classroom_read","level_read","period_read","period_write"})
     * 
     * @Assert\NotBlank(message="The end hour cannot be blank")
     * @Assert\NotNull(message="The end hour is mandatory")
     */
    private $endHour;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"classroom_read","level_read","period_read","period_write"})
     * 
     * @Assert\NotBlank(message="The course cannot be blank")
     * @Assert\NotNull(message="The course is mandatory")
     */
    private $course;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"classroom_read","level_read","period_read","period_write"})
     * 
     * @Assert\NotBlank(message="The state cannot be blank")
     * @Assert\NotNull(message="The state is mandatory")
     */
    private $state;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"classroom_read","level_read","period_read","period_write"})
     * 
     * @Assert\NotBlank(message="The official cannot be blank")
     * @Assert\NotNull(message="The official is mandatory")
     */
    private $official;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"classroom_read","level_read","period_read","period_write"})
     * 
     * @Assert\NotBlank(message="The day cannot be blank")
     * @Assert\NotNull(message="The day is mandatory")
     */
    private $day;

    /**
     * @ORM\ManyToOne(targetEntity=Classroom::class, inversedBy="periods")
     * @Groups({"period_read","period_write"})
     * 
     * @Assert\NotNull(message="Each program is linking to classroom. Provide the classroom")
     */
    private $classroom;

    /**
     * @ORM\ManyToOne(targetEntity=Level::class, inversedBy="periods")
     * @Groups({"period_read"})
     * 
     * @Assert\NotNull(message="Each program is linking to level. Provide the level")
     * 
     */
    private $level;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeginHour(): ?string
    {
        return $this->beginHour;
    }

    public function setBeginHour(string $beginHour): self
    {
        $this->beginHour = $beginHour;

        return $this;
    }

    public function getEndHour(): ?string
    {
        return $this->endHour;
    }

    public function setEndHour(string $endHour): self
    {
        $this->endHour = $endHour;

        return $this;
    }

    public function getCourse(): ?string
    {
        return $this->course;
    }

    public function setCourse(string $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getState(): ?bool
    {
        return $this->state;
    }

    public function setState(bool $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getOfficial(): ?bool
    {
        return $this->official;
    }

    public function setOfficial(bool $official): self
    {
        $this->official = $official;

        return $this;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getClassroom(): ?Classroom
    {
        return $this->classroom;
    }

    public function setClassroom(?Classroom $classroom): self
    {
        $this->classroom = $classroom;

        return $this;
    }

    public function getLevel(): ?Level
    {
        return $this->level;
    }

    public function setLevel(?Level $level): self
    {
        $this->level = $level;

        return $this;
    }
}
