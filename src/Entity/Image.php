<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ImageRepository;
use App\Entity\Traits\Timestampable;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ImageRepository::class)
 * @ORM\Table(name="mh_images")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource()
 * 
 */
class Image
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"institution_read","campus_read","classroom_read","faculty_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)\
     * @Groups({"institution_read","campus_read","classroom_read","faculty_read","faculty_write"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"institution_read","campus_read","classroom_read","faculty_read","faculty_write"})
     */
    private $src;

    /**
     * @ORM\ManyToOne(targetEntity=Institution::class, inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $institution;

    /**
     * @ORM\ManyToOne(targetEntity=Campus::class, inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $campus;

    /**
     * @ORM\ManyToOne(targetEntity=Classroom::class, inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classroom;

    /**
     * @ORM\ManyToOne(targetEntity=Faculty::class, inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $faculty;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSrc(): ?string
    {
        return $this->src;
    }

    public function setSrc(string $src): self
    {
        $this->src = $src;

        return $this;
    }

    public function getInstitution(): ?Institution
    {
        return $this->institution;
    }

    public function setInstitution(?Institution $institution): self
    {
        $this->institution = $institution;

        return $this;
    }

    public function getCampus(): ?Campus
    {
        return $this->campus;
    }

    public function setCampus(?Campus $campus): self
    {
        $this->campus = $campus;

        return $this;
    }

    public function getClassroom(): ?Classroom
    {
        return $this->classroom;
    }

    public function setClassroom(?Classroom $classroom): self
    {
        $this->classroom = $classroom;

        return $this;
    }

    public function getFaculty(): ?Faculty
    {
        return $this->faculty;
    }

    public function setFaculty(?Faculty $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }
}
