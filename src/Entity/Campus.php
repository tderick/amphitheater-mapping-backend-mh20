<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\CampusRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=CampusRepository::class)
 * @ORM\Table(name="mh_campus")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"campus_read"}},
 *      denormalizationContext={"groups"={"campus_write"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 */
class Campus
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"institution_read","campus_read","classroom_read","campus_write","classroom_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"institution_read","campus_read","classroom_read","campus_write"})
     * 
     * @Assert\NotBlank(message="The name of campus cannot be blank")
     * @Assert\NotNull(message="The name of campus is mandatory")

     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Institution::class, inversedBy="campuses")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"campus_read","campus_write"})
     * @Assert\NotNull(message="You precise the institution of the campus")
     */
    private $institution;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="campus", orphanRemoval=true)
     * @Groups({"campus_read"})
     * 
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity=Classroom::class, mappedBy="campus", orphanRemoval=true)
     * @Groups({"campus_read"})
     * 
     */
    private $classrooms;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->classrooms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getInstitution(): ?Institution
    {
        return $this->institution;
    }

    public function setInstitution(?Institution $institution): self
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCampus($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getCampus() === $this) {
                $image->setCampus(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Classroom[]
     */
    public function getClassrooms(): Collection
    {
        return $this->classrooms;
    }

    public function addClassroom(Classroom $classroom): self
    {
        if (!$this->classrooms->contains($classroom)) {
            $this->classrooms[] = $classroom;
            $classroom->setCampus($this);
        }

        return $this;
    }

    public function removeClassroom(Classroom $classroom): self
    {
        if ($this->classrooms->removeElement($classroom)) {
            // set the owning side to null (unless already changed)
            if ($classroom->getCampus() === $this) {
                $classroom->setCampus(null);
            }
        }

        return $this;
    }
}
