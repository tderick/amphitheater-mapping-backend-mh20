<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\DepartmentRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=DepartmentRepository::class)
 * @ORM\Table(name="mh_departements")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"department_read"}},
 *      denormalizationContext={"groups"={"department_write"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * 
 */
class Department
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"faculty_read","department_read","speciality_read","department_write","speciality_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"faculty_read","department_read","speciality_read","department_write"})
     * 
     * @Assert\NotBlank(message="The name of department cannot be blank")
     * @Assert\NotNull(message="The name of department is mandatory")
     * 
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"faculty_read","department_read","speciality_read","department_write"})
     * 
     * @Assert\NotBlank(message="The name of headman cannot be blank")
     * @Assert\NotNull(message="The name of headman is mandatory")
     * 
     */
    private $headman;

    /**
     * @ORM\ManyToOne(targetEntity=Faculty::class, inversedBy="departments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"department_read","department_write"})
     * @Assert\NotNull(message="You must precise the faculty of the department")
     */
    private $faculty;

    /**
     * @ORM\OneToMany(targetEntity=Speciality::class, mappedBy="department", orphanRemoval=true)
     * @Groups({"department_read"})
     */
    private $specialities;

    public function __construct()
    {
        $this->specialities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHeadman(): ?string
    {
        return $this->headman;
    }

    public function setHeadman(string $headman): self
    {
        $this->headman = $headman;

        return $this;
    }

    public function getFaculty(): ?Faculty
    {
        return $this->faculty;
    }

    public function setFaculty(?Faculty $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

    /**
     * @return Collection|Speciality[]
     */
    public function getSpecialities(): Collection
    {
        return $this->specialities;
    }

    public function addSpeciality(Speciality $speciality): self
    {
        if (!$this->specialities->contains($speciality)) {
            $this->specialities[] = $speciality;
            $speciality->setDepartment($this);
        }

        return $this;
    }

    public function removeSpeciality(Speciality $speciality): self
    {
        if ($this->specialities->removeElement($speciality)) {
            // set the owning side to null (unless already changed)
            if ($speciality->getDepartment() === $this) {
                $speciality->setDepartment(null);
            }
        }

        return $this;
    }
}
