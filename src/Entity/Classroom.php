<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\ClassroomRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=ClassroomRepository::class)
 * @ORM\Table(name="mh_classrooms")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"classroom_read"}},
 *      denormalizationContext={"groups"={"classroom_write"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 */
class Classroom
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"campus_read","classroom_read","period_read","classroom_write","period_write"})
     * 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"campus_read","classroom_read","classroom_write"})
     * 
     * @Assert\NotBlank(message="The name of classroom cannot be blank")
     * @Assert\NotNull(message="The name of classroom is mandatory")

     * 
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"campus_read","classroom_read","period_read","classroom_write"})
     * 
     */
    private $capacity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"campus_read","classroom_read","period_read","period_read","classroom_write"})
     * 
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=Campus::class, inversedBy="classrooms")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"classroom_read","classroom_write"})
     * 
     * @Assert\NotNull(message="You must precise the campus of the classroom")
     */
    private $campus;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="classroom", orphanRemoval=true)
     * @Groups({"classroom_read","classroom_write"})
     * 
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity=Period::class, mappedBy="classroom")
     * @Groups({"classroom_read"})
     * 
     */
    private $periods;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->periods = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(?int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getCampus(): ?Campus
    {
        return $this->campus;
    }

    public function setCampus(?Campus $campus): self
    {
        $this->campus = $campus;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setClassroom($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getClassroom() === $this) {
                $image->setClassroom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Period[]
     */
    public function getPeriods(): Collection
    {
        return $this->periods;
    }

    public function addPeriod(Period $period): self
    {
        if (!$this->periods->contains($period)) {
            $this->periods[] = $period;
            $period->setClassroom($this);
        }

        return $this;
    }

    public function removePeriod(Period $period): self
    {
        if ($this->periods->removeElement($period)) {
            // set the owning side to null (unless already changed)
            if ($period->getClassroom() === $this) {
                $period->setClassroom(null);
            }
        }

        return $this;
    }
}
