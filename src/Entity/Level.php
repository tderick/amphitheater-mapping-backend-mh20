<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\LevelRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=LevelRepository::class)
 * @ORM\Table(name="mh_levels")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"level_read"}},
 *      denormalizationContext={"groups"={"level_write"}},
 * 
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * 
 */
class Level
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"speciality_read","level_read","period_read","level_write","period_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"speciality_read","level_read","period_read","level_write"})
     * 
     * @Assert\NotBlank(message="The name of level cannot be blank")
     * @Assert\NotNull(message="The name of level is mandatory")
     * 
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Speciality::class, inversedBy="levels")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"level_read","level_write"})
     * 
     * @Assert\NotNull(message="You must precise the speciality of level")
     */
    private $speciality;

    /**
     * @ORM\OneToMany(targetEntity=Period::class, mappedBy="level")
     * @Groups({"level_read"})
     * 
     */
    private $periods;

    public function __construct()
    {
        $this->periods = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSpeciality(): ?Speciality
    {
        return $this->speciality;
    }

    public function setSpeciality(?Speciality $speciality): self
    {
        $this->speciality = $speciality;

        return $this;
    }

    /**
     * @return Collection|Period[]
     */
    public function getPeriods(): Collection
    {
        return $this->periods;
    }

    public function addPeriod(Period $period): self
    {
        if (!$this->periods->contains($period)) {
            $this->periods[] = $period;
            $period->setLevel($this);
        }

        return $this;
    }

    public function removePeriod(Period $period): self
    {
        if ($this->periods->removeElement($period)) {
            // set the owning side to null (unless already changed)
            if ($period->getLevel() === $this) {
                $period->setLevel(null);
            }
        }

        return $this;
    }
}
