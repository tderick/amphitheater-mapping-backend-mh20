<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\SpecialityRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=SpecialityRepository::class)
 * @ORM\Table(name="mh_specialities")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"speciality_read"}},
 *      denormalizationContext={"groups"={"speciality_write"}},
 * 
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * 
 */
class Speciality
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"department_read","speciality_read","level_read","speciality_write","level_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"department_read","speciality_read","level_read","speciality_write"})
     * 
     * @Assert\NotBlank(message="The name of speciality cannot be blank")
     * @Assert\NotNull(message="The name of speciality is mandatory")
     * 
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Department::class, inversedBy="specialities")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"speciality_read","speciality_write"})
     * 
     * @Assert\NotNull(message="You must precise de department of speciality")
     */
    private $department;

    /**
     * @ORM\OneToMany(targetEntity=Level::class, mappedBy="speciality", orphanRemoval=true)
     * @Groups({"speciality_read"})
     */
    private $levels;

    public function __construct()
    {
        $this->levels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return Collection|Level[]
     */
    public function getLevels(): Collection
    {
        return $this->levels;
    }

    public function addLevel(Level $level): self
    {
        if (!$this->levels->contains($level)) {
            $this->levels[] = $level;
            $level->setSpeciality($this);
        }

        return $this;
    }

    public function removeLevel(Level $level): self
    {
        if ($this->levels->removeElement($level)) {
            // set the owning side to null (unless already changed)
            if ($level->getSpeciality() === $this) {
                $level->setSpeciality(null);
            }
        }

        return $this;
    }
}
