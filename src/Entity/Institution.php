<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\InstitutionRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InstitutionRepository::class)
 * @ORM\Table(name="mh_institutions")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"institution_read"}},
 *      collectionOperations={"GET","POST"},
 *      itemOperations={"GET","DELETE","PUT"}
 * )
 * 
 */
class Institution
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"institution_read","campus_read","faculty_read","campus_write","faculty_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The name cannot be blank")
     * @Assert\NotNull(message="The name is mandatory")
     * @Groups({"institution_read","campus_read","faculty_read"})
     * 
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The localisation cannot be blank")
     * @Assert\NotNull(message="The localisation is mandatory")
     * @Groups({"institution_read","campus_read","faculty_read"})
     * 
     */
    private $localisation;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The POBOX cannot be blank")
     * @Assert\NotNull(message="The POBOX is mandatory")
     * @Groups({"institution_read","campus_read","faculty_read"})
     */
    private $POBOX;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="The name of vice-chancellor cannot be blank")
     * @Assert\NotNull(message="The name of vice-chancellor  is mandatory")
     * @Groups({"institution_read","campus_read","faculty_read"})
     */
    private $viceChancellor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"institution_read"})
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"institution_read"})
     */
    private $logo;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="institution", orphanRemoval=true)
     * @Groups({"institution_read"})
     * 
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="institutions")
     * @Groups({"institution_read"})
     * 
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Campus::class, mappedBy="institution", orphanRemoval=true)
     * @Groups({"institution_read"})
     */
    private $campuses;

    /**
     * @ORM\OneToMany(targetEntity=Faculty::class, mappedBy="institution", orphanRemoval=true)
     * @Groups({"institution_read"})
     * 
     */
    private $faculties;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->campuses = new ArrayCollection();
        $this->faculties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getPOBOX(): ?string
    {
        return $this->POBOX;
    }

    public function setPOBOX(string $POBOX): self
    {
        $this->POBOX = $POBOX;

        return $this;
    }

    public function getViceChancellor(): ?string
    {
        return $this->viceChancellor;
    }

    public function setViceChancellor(string $viceChancellor): self
    {
        $this->viceChancellor = $viceChancellor;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setInstitution($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getInstitution() === $this) {
                $image->setInstitution(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return Collection|Campus[]
     */
    public function getCampuses(): Collection
    {
        return $this->campuses;
    }

    public function addCampus(Campus $campus): self
    {
        if (!$this->campuses->contains($campus)) {
            $this->campuses[] = $campus;
            $campus->setInstitution($this);
        }

        return $this;
    }

    public function removeCampus(Campus $campus): self
    {
        if ($this->campuses->removeElement($campus)) {
            // set the owning side to null (unless already changed)
            if ($campus->getInstitution() === $this) {
                $campus->setInstitution(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Faculty[]
     */
    public function getFaculties(): Collection
    {
        return $this->faculties;
    }

    public function addFaculty(Faculty $faculty): self
    {
        if (!$this->faculties->contains($faculty)) {
            $this->faculties[] = $faculty;
            $faculty->setInstitution($this);
        }

        return $this;
    }

    public function removeFaculty(Faculty $faculty): self
    {
        if ($this->faculties->removeElement($faculty)) {
            // set the owning side to null (unless already changed)
            if ($faculty->getInstitution() === $this) {
                $faculty->setInstitution(null);
            }
        }

        return $this;
    }
}
