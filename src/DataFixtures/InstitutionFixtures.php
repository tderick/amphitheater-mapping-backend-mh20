<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Campus;
use App\Entity\Faculty;
use App\Entity\Classroom;
use App\Entity\Department;
use App\Entity\Institution;
use App\Entity\Level;
use App\Entity\Period;
use App\Entity\Speciality;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class InstitutionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $faker = Factory::create();

        $university = new Institution;
        $university->setName("University of Dschang")
            ->setLocalisation("DSCHANG")
            ->setPOBOX("BP: 54 DSCHANG")
            ->setViceChancellor("Prof NANFOSSO TSAFACK ")
            ->setWebsite($faker->domainName)
        ;

        $manager->persist($university);

        $campusA = new Campus;
        $campusA->setName("Campus A");
        $university->addCampus($campusA);
        $manager->persist($campusA);

        $campusC = new Campus;
        $campusC->setName("Campus C");
        $university->addCampus($campusC);
        $manager->persist($campusC);

        // Ajout des salles dans le campus A
        for ($i=1; $i < 5; $i++) { 
            $salle = new Classroom;
            $salle->setName("SN1 00".$i)
            ->setCapacity("100");
            $campusA->addClassroom($salle);
            $manager->persist($salle);
        }

        for ($i=1; $i < 5; $i++) { 
            $salle = new Classroom;
            $salle->setName("SN2 00".$i)
            ->setCapacity("100");

            $campusA->addClassroom($salle);
            $manager->persist($salle);
        }

        for ($i=1; $i < 5; $i++) { 
            $salle = new Classroom;
            $salle->setName("B1 10".$i)
            ->setCapacity("100");

            $campusA->addClassroom($salle);
            $manager->persist($salle);
        }

        for ($i=1; $i < 5; $i++) { 
            $salle = new Classroom;
            $salle->setName("B2 10".$i)
            ->setCapacity("100");
            $campusA->addClassroom($salle);
            $manager->persist($salle);
        }

        for ($i=1; $i < 5; $i++) { 
            $salle = new Classroom;
            $salle->setName("B3 10".$i);
            $campusA->addClassroom($salle);
            $manager->persist($salle);
        }

        for ($i=0; $i < 4; $i++) { 
            $salle = new Classroom;
            $salle->setName("Amphi 35".$i);
            $campusA->addClassroom($salle);
            $manager->persist($salle);
        }

        for ($i=0; $i < 4; $i++) { 
            $salle = new Classroom;
            $salle->setName("AMPHI 45".$i);
            $campusA->addClassroom($salle);
            $manager->persist($salle);
        }

        // Ajout des salles dans le campus B
        for ($i=4; $i < 6; $i++) { 
            $salle = new Classroom;
            $salle->setName("AMPHI 35".$i);
            $campusC->addClassroom($salle);
            $manager->persist($salle);
        }

        $salle = new Classroom;
        $salle->setName("AMPHI 600")
            ->setCapacity("600");
        $campusC->addClassroom($salle);

        $manager->persist($salle);


        // ADD FACULTY
        $facScience = new Faculty;
        $facScience->setName("FACULTE DES SCIENCES")
            ->setDeanName("NGAMENI EMMANUEL")
            ->setWebsite($faker->domainName)
        ;
        $manager->persist($facScience);
        $university->addFaculty($facScience);

        $facLettre = new Faculty;
        $facLettre->setName("FACULTE DES LETTRES")
            ->setDeanName($faker->name())
            ->setWebsite($faker->domainName)
        ;
        $manager->persist($facLettre);
        $university->addFaculty($facLettre);

        $facDroit = new Faculty;
        $facDroit->setName("FACULTE DE DROITS")
            ->setDeanName($faker->name())
            ->setWebsite($faker->domainName)
        ;
        $manager->persist($facDroit);
        $university->addFaculty($facDroit);

        $facSeco = new Faculty;
        $facSeco->setName("FACULTE DE SCIENCE ECONOMIQUE ET DE GESTION")
            ->setDeanName($faker->name())
            ->setWebsite($faker->domainName)
        ;
        $manager->persist($facSeco);
        $university->addFaculty($facSeco);


        //ADD  DEPARTMENT
        $mathinfo = new Department;
        $mathinfo->setName("DEPARTEMENT DE MATH-INFOS")
            ->setHeadman("Prof NKENLIFACK MARCELIN");
        $manager->persist($mathinfo);
        $facScience->addDepartment($mathinfo);

        $physique = new Department;
        $physique->setName("DEPARTEMENT DE PHYSIQUE")
            ->setHeadman($faker->name());
        $manager->persist($physique);
        $facScience->addDepartment($physique);

        $chimie = new Department;
        $chimie->setName("DEPARTEMENT DE CHIMIE")
            ->setHeadman($faker->name());
        $manager->persist($chimie);
        $facScience->addDepartment($chimie);

        //Add speciality
        $math = new Speciality;
        $math->setName("MATHEMATIQUES");
        $manager->persist($math);
        $mathinfo->addSpeciality($math);

        $info = new Speciality;
        $info->setName("INFORMATIQUES");
        $manager->persist($info);
        $mathinfo->addSpeciality($info);

        $ch= new Speciality;
        $ch->setName("CHIMIE");
        $manager->persist($ch);
        $chimie->addSpeciality($ch);

        $ph= new Speciality;
        $ph->setName("PHYSIQUE");
        $manager->persist($ph);
        $physique->addSpeciality($ph);

        //Add level
        for ($i=1; $i < 6; $i++) { 
            $level1 = new Level;
            $level1->setName("MA".$i);
            $manager->persist($level1);
            $math->addLevel($level1);

            $prog1 = new Period;
            $prog1->setBeginHour("8H00")
                ->setEndHour("10H00")
                ->setCourse("MAT 311 : Topologie Générale")
                ->setState(true)
                ->setOfficial(true)
                ->setDay("LUNDI")
            ;

            $manager->persist($prog1);
            $salle->addPeriod($prog1);
            $level1->addPeriod($prog1);

            $level2 = new Level;
            $level2->setName("IN".$i);
            $manager->persist($level2);
            $info->addLevel($level2);

            $level3 = new Level;
            $level3->setName("CH".$i);
            $manager->persist($level3);
            $ch->addLevel($level3);

            $level4 = new Level;
            $level4->setName("PH".$i);
            $manager->persist($level4);
            $ph->addLevel($level4);
        }

        //ADD Programme

        


        $manager->flush();
    }
}

